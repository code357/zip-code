jQuery(function ($) {
    let checkout_form = $('form.checkout');
    checkout_form.on('checkout_place_order', function (e) {
        e.preventDefault()
        let billingPostcode = document.getElementById('billing_postcode');
        let zip_code_list = validationObj.zip_code_list;
        checkout_form.remove('#zip_code_list');
        checkout_form.append('<input type="hidden" name="zip_code_list" id="zip_code_list" value="'+zip_code_list.zip_codes_list_input+'">');
        let acceptedZipCodes = document.querySelector('#zip_code_list').value;
        let zip_codes_to_arr = acceptedZipCodes.split(',');
        let prevent_submit = document.getElementById('prevent_submit');

        if (zip_codes_to_arr.indexOf(billingPostcode.value) === -1) {

            checkout_form.append('<input type="hidden" name="prevent_submit" id="prevent_submit" value="1">');
        } else {
            prevent_submit.remove();
        }
    });
});


