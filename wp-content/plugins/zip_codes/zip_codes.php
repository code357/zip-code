<?php
/**
 * Plugin Name: ZipCodes
 */

class zipCodes {

	public function __construct() {

		add_action( 'admin_init', array( $this, 'zip_codes_settings_init' ) );
		add_action( 'admin_menu', array( $this, 'zip_codes_options_page' ), 10 );
		add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ), 10 );
		add_filter( 'woocommerce_checkout_fields', array( $this, 'no_zip_validation' ) );
		add_action( 'woocommerce_after_checkout_validation', array( $this, 'custom_zip_validation' ), 10, 2 );
		add_action( 'woocommerce_after_checkout_validation', array( $this, 'prevent_submit' ) );

	}


	function load_scripts(): void {
		if ( is_checkout() ) {
			wp_enqueue_script( 'validate-js', plugins_url( 'assets/js/custom.js', __FILE__ ), array( 'jquery' ), '1.0.0', true );
			wp_localize_script( 'validate-js', 'validationObj', array(
				'zip_code_list' => get_option( 'zip_codes_options' ),
			) );
		}
	}


	function zip_codes_options_page(): void {
		add_menu_page(
			'Zip Codes',
			'Zip Codes Options',
			'manage_options',
			'zip-codes',
			array( $this, 'zip_codes_options_page_html' )
		);
	}


	function zip_codes_options_page_html(): void {

		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		if ( isset( $_GET['settings-updated'] ) ) {

			add_settings_error( 'zip_code_messages', '_message', __( 'Settings Saved', 'zip_codes' ), 'updated' );
		}


		settings_errors( 'zip_code_messages' );
		?>
		<div class="wrap">
			<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<form action="options.php" method="post">
				<?php
				settings_fields( 'zip_codes' );
				do_settings_sections( 'zip_codes' );
				submit_button( 'Save Settings' );
				?>
			</form>
		</div>
		<?php
	}


	function zip_codes_settings_init(): void {

		register_setting( 'zip_codes', 'zip_codes_options' );

		add_settings_section(
			'zip_codes_section',
			'',
			'',
			'zip_codes'
		);

		add_settings_field(
			'text_area_zip_codes',
			__( 'Comma-sepparated list', 'zip_codes' ),
			array( $this, 'zip_codes_list_input' ),
			'zip_codes',
			'zip_codes_section',
			array(
				'label_for'   => 'zip_codes_list_input',
				'class'       => 'row',
				'custom_data' => 'custom',
			)
		);
	}


	function zip_codes_list_input( $args ): void {

		$options = get_option( 'zip_codes_options' );
		?>
		<input
			id="<?php echo esc_attr( $args['label_for'] ); ?>"
			data-custom="<?php echo esc_attr( $args['custom_data'] ); ?>"
			name="zip_codes_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
			value="<?php echo esc_attr( trim( $options[ $args['label_for'] ] ) ); ?>" style="width: 100%"/>


		<?php
	}

	function no_zip_validation( $fields ) {

		unset( $fields['billing']['billing_postcode']['validate'] );
		unset( $fields['shipping']['shipping_postcode']['validate'] );

		return $fields;
	}


	function custom_zip_validation( $fields, $errors ): void {

		$option        = get_option( 'zip_codes_options' );
		$zip_code_list = explode( ',', $option['zip_codes_list_input'] );

//		if ( preg_match( '/\\D/', $fields['billing_postcode'] )  ) {
//			$errors->add( 'validation', '<strong>Billing ZIP</strong> Your zip code contains a letter(s). ' );
//		}

		if ( preg_match( '/\\D/', $fields['shipping_postcode'] ) ) {
			$errors->add( 'validation', '<strong>Shipping ZIP</strong> Your zip code contains a letter(s). ' );
		}

//		if ( ! in_array( $fields['billing_postcode'], $zip_code_list ) ) {
//			$errors->add( 'validation', '<strong>Billing ZIP</strong> Not in the list of accepted zip codes' );
//		}

		if ( ! in_array( $fields['shipping_postcode'], $zip_code_list ) ) {
			$errors->add( 'validation', '<strong>Shipping ZIP</strong> Not in the list of accepted zip codes' );

		}

	}


	function prevent_submit( $posted ): void {
		if ( isset( $_POST['prevent_submit'] ) && wc_notice_count( 'error' ) == 0 ) {
			wc_add_notice( __( "<strong>Billing Zip </strong>Not in the list of accepted codes", 'zip_codes' ), 'error' );

		}
	}


}

$zip_codes = new zipCodes();